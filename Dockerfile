FROM nginx
COPY build /usr/share/nginx/html

COPY conf/mime.types /etc/nginx/mime.types

# gzip
COPY conf/gzip.conf /etc/nginx/conf.d/gzip.conf

# https://geekflare.com/secure-mime-types-in-apache-nginx-with-x-content-type-options/
# http://blog.jambura.com/2015/01/15/add-extra-security-to-nginx-to-stop-clickjacking-xss-protection/#sthash.lujzQfAL.dpuf
COPY conf/security.conf /etc/nginx/conf.d/security.conf

COPY conf/default.conf /etc/nginx/conf.d/default.conf
