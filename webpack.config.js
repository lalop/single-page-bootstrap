
var cssnext = require('postcss-cssnext'),
    lost = require('lost'),
    bemLinter = require('postcss-bem-linter'),
    atImport = require('postcss-import'),
    fontMagician = require('postcss-font-magician'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    path = require('path'),
    srcPath = path.join(__dirname, 'src'),
    distPath = path.join(__dirname, 'build'),
    webpack = require('webpack'),
    yaml = require('js-yaml'),
    fs   = require('fs'),
    tplData = yaml.safeLoad(fs.readFileSync(path.join(srcPath, 'tpl-data.yml'), 'utf8'));
//    easysprite = require('postcss-easysprites');

module.exports = {

    entry: path.join(srcPath, 'js', 'main.js'),

    output: {
        path: distPath,
        filename: 'app.js?[hash]',
        publicPath: "/"
    },

    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader')
            },
            {
                test: /\.woff2?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?name=img/[name].[ext]?[hash]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {
              test: /\.js$/,
              exclude: /(node_modules)/,
              loader: 'babel-loader?presets[]=es2015'
            },
            {
              test: /\.html$/,
              loader: "handlebars-loader"
            }
        ]
    },

    plugins: [
        new ExtractTextPlugin('[name].css?[hash]'),
        new HtmlWebpackPlugin({
            template: path.join(srcPath, 'index.html'),
            inject: true,
            data: tplData
        })
    //     ,
    //     new webpack.DefinePlugin({
    //       "process.env": {
    //         NODE_ENV: JSON.stringify("production")
    //       }
    //   })
    ],

    postcss: function (webpack) {
        return [
            atImport({
                addDependencyTo: webpack
            }),
            lost, bemLinter, cssnext
            //,
            // easysprite({
            //     imagePath:'./src/img',
            //     spritePath: './src/img'
            // })
        ];
    },

    resolve: {modulesDirectories: ['node_modules']},
    devtool: "#source-map"
}
