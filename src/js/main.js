require('../css/main.css');
require('leaflet/dist/leaflet.css');
require('./classlist-polyfill');
import projets from './projet';
import "babel-polyfill";

document.addEventListener('DOMContentLoaded', function () {

    var  L = require('leaflet'),
        mapOpt = {
            zoomControl: false,
            dragging: false,
            touchZoom: false,
            scrollWheelZoom: false,
            doubleClickZoom: false
        },
        map = L.map('map-wrapper', mapOpt)
                .setView([48.8510, 2.3094], 7);


    // L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png')
    //     .addTo(map);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
        .addTo(map);

    var icon = L.icon({
        iconUrl: require('../img/point.svg'),
        iconSize: [38, 95]
    });

    L.marker([48.8510, 2.3094], {icon: icon})
        .addTo(map);

    projets(document.querySelectorAll('#projets article'));

});
