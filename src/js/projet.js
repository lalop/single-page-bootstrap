
import {lory} from 'lory.js';

//detect transition support
function detectCSSFeature(featurename){
    var feature = false,
    domPrefixes = 'Webkit Moz ms O'.split(' '),
    elm = document.createElement('div'),
    featurenameCapital = null;

    featurename = featurename.toLowerCase();

    if( elm.style[featurename] !== undefined ) { feature = true; }

    if( feature === false ) {
        featurenameCapital = featurename.charAt(0).toUpperCase() + featurename.substr(1);
        for( var i = 0; i < domPrefixes.length; i++ ) {
            if( elm.style[domPrefixes[i] + featurenameCapital ] !== undefined ) {
              feature = true;
              break;
            }
        }
    }
    return feature;
}

var hasCssTransitionSupport = detectCSSFeature("transition");


export default function(els) {

    const popin = document.getElementById('projet-popin');
    let slider;

    Array.prototype.forEach.call(els, el => {
        el.addEventListener("click", e => openDetail(e, el.getAttribute('data-project')), false)
    });

    function openDetail(e, project) {
        let doc = document.documentElement,
            body = document.getElementsByTagName('body')[0],
            width = window.innerWidth || doc.clientWidth || body.clientWidth,
            height = window.innerHeight || doc.clientHeight || body.clientHeight,
            maxDistX = Math.max(e.clientX, width - e.clientX),
            maxDistY = Math.max(e.clientY, height - e.clientY),
            animSize = Math.sqrt(maxDistX * maxDistX + maxDistY * maxDistY);

        if(hasCssTransitionSupport) {
            if(e && e.clientX) {
                popin.style.left = `${e.clientX}px`;
                popin.style.right = `${width - e.clientX}px`;
                popin.style.top = `${e.clientY}px`;
                popin.style.bottom = `${height - e.clientY}px`;
            }
            popin.classList.add("animate");

            setTimeout(() => startAnimation({
                left: `${e.clientX - animSize}px`,
                right: `${width - e.clientX - animSize}px`,
                top: `${e.clientY - animSize}px`,
                bottom: `${height - e.clientY - animSize}px`,
                opacity: 1
            }), 10);
        } else {
            animationCompleted()
        }

        if(slider === undefined) {
            slider = lory(document.querySelector('.slider'), {
                enableMouseEvents: true,
                rewind: true
            });
        }

        let currentSlide = Array.prototype.map.call(
            document.querySelectorAll('.frame li'),
            node => {return node.getAttribute('data-project')}
        ).indexOf(project);

        slider.slideTo(currentSlide);

        [].forEach.call(document.querySelectorAll('.js_slide img'), (el) => {
            el.addEventListener('mousedown', (e) => {
                e.preventDefault();
            }, true);
        });

        for(let bt of document.querySelectorAll('.popin .js_close')) {
            bt.addEventListener('click', closeDetail);
        }

    }

    function startAnimation(animTo) {
        for(let property in animTo) {
            popin.style[property] = animTo[property];
        }
        popin.addEventListener('transitionend', animationCompleted, true);
    }

    function animationCompleted(e) {
        if(!e || e.propertyName === 'top') {
            popin.removeEventListener('transitionend', this.animationCompleted);
            ['left', 'right', 'top', 'bottom', 'opacity']
                .forEach(popin.style.removeProperty.bind(popin.style));
            popin.classList.remove("animate");
            popin.classList.add("open");
        }
    }

    function closeDetail() {
        popin.classList.remove("open");
    //    slider.destroy();
    //    slider = undefined;
    //    for(let bt of document.querySelectorAll('.popin .js_close')) {
    //        bt.removeEventListener('click', () => this.closeDetail());
    //    }
    }
}
